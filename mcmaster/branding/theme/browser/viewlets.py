from Acquisition import aq_inner
from zope.component import getUtility
from zope.component import getMultiAdapter

from Products.CMFCore.ActionInformation import ActionInfo
from Products.CMFCore.interfaces import ISiteRoot
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from plone.app.layout.viewlets.common import PathBarViewlet
from plone.app.layout.viewlets.common import SearchBoxViewlet \
    as SearchBoxViewletBase


class SearchBoxViewlet(SearchBoxViewletBase):
    index = ViewPageTemplateFile('custom/searchbox.pt')

    def update(self):
        super(SearchBoxViewlet, self).update()

        portal = getUtility(ISiteRoot)
        self.portal_title = portal.Title()

        self.portal_short_title = portal.getProperty('short_title',
                                                     portal.Title())

        self.portal_short_title = \
            (len(self.portal_short_title) < 17) \
            and self.portal_short_title \
            or ''.join([word[0] for word in self.portal_short_title.split()
                        if word[0].isupper()])

    def quick_links(self):
        pa = getToolByName(self.context, 'portal_actions')
        ql_cat = pa._getOb('mcmaster_quick_links')
        ec = pa._getExprContext(ql_cat)
        for action in ql_cat.listActions():
            yield ActionInfo(action, ec)


class McMasterPathBar(PathBarViewlet):
    index = ViewPageTemplateFile('custom/path_bar.pt')

    def portalRootCSSMarker(self):
        plone_view = getMultiAdapter((aq_inner(self.context), self.request,),
                                     name="plone")

        if plone_view.isPortalOrPortalDefaultPage():
            return 'frontPage'
        return None
