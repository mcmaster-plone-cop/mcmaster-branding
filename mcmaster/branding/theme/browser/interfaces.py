from plonetheme.classic.browser.interfaces import (
    IThemeSpecific as IClassicTheme,
)


class IThemeSpecific(IClassicTheme):
    """Marker interface that defines a Zope 3 browser layer.
       If you need to register a viewlet only for the
       "McMaster University branding" theme, this interface must be its layer
       (in theme/viewlets/configure.zcml).
    """
