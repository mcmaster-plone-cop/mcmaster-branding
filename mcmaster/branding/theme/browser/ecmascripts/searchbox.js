function updateLiveSearchStatus() {
    search_local_element = jQuery("#search_scope_local")[0];
    if (! search_local_element) return
    if (typeof livesearch._real_search == "undefined") {
        livesearch._real_search = livesearch.search;
    }
    searchInput = jQuery("#searchGadget")[0];
    if (search_local_element.checked) {
	livesearch.search = livesearch._real_search;
        searchInput.form.action = "search";
        searchInput.name = "SearchableText";
    }
    else {
        livesearch.search = function () {};
        searchInput.form.action = "http://www.mcmaster.ca/opr/html/mcmaster_home/main/search_results.html";
        searchInput.name = "q";
    }
}

jQuery( function() {
    focusSearchInput = function () {
        jQuery("#searchGadget")[0].focus();
    };

    jQuery("#search_scope_local, #search_scope_university").each(
      function () {
        jQuery(this).change(updateLiveSearchStatus);
        jQuery(this).parent().click(focusSearchInput);
    });
});
