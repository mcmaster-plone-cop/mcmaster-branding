/*
 * Takes care of second level menus that should open to the left to
 * keep the illusion of the invisible box.
 */
jQuery( function() {
    var gn = jQuery('#portal-globalnav');

    // We can't know the real dimension until the sub menus are
    // unfolded on mouse over.
    jQuery('#portal-globalnav > LI > UL > LI').mouseenter(
	function() {
	    var sub = jQuery(this).children('ul:not(.dropToLeft)');
	    if (sub.length == 0) return;
	    // Check if the submenu's right edge is outside the global
	    // navigation's right edge.
	    if ( (gn.offset().left + gn.width()) < (sub.offset().left + sub.width()) )
	    {
		sub.addClass('dropToLeft');
		sub.find('LI > UL').addClass('dropToLeft');
	    };
	});
});
