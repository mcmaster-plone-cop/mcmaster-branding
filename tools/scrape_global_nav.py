"""A simple spider that extracts the global navigation artifacts.

The global navigation is scraped from the Faculty of Medicine home
page, since that one seem to be the most up-to-date.

The output of this script are the data structures to be pasted into
the browser/viewlets.py file in order to update the artifacts in the
theme.

This requires the Scrapy framework (scrapy.org) to be installed.

Also, Python 2.7+ is required, as there seems to exist a bug in Python
2.6 that makes it successful to test lxml.builder.E with
inspect.isclass but then issubclass fails with a TypeError saying it
isn't. This check is used by the runspider command of Scrapy.

Usage:

$ scrapy runspider -a actions_file=/path/to/actions.xml scrape_global_nav.py

"""

import re
import subprocess
import sys

import lxml.etree
from lxml.builder import E
from scrapy.exceptions import DropItem
from scrapy.item import (
    Item,
    Field,
)
from scrapy.selector import HtmlXPathSelector
from scrapy.spider import BaseSpider
import scrapy.settings.default_settings


assert sys.version_info[0:2] < (2, 7), \
    'Python version < 2.7 has issubclass bug.'


def normalize(non_normalized):
    """Normalizes a string for use as a Zope/Plone object ID.

    @todo: Use plone.i18n.normalizer
    """
    return re.sub("-+", "-",
                  re.sub("\W", "-",
                         non_normalized.lower()))


def menu_dom2actionsXML(dom_menu, xml_menu):
    """From an HTML menu build a Plone actions.xml-ready structure.

    The structure is that expected by quintagroup.dropdownmenu, in a
    format compatible for a GenericSetup import file (actions.xml).

    The dom_menu is expected to be either of type HtmlXPathSelector or
    XPathSelectorList.

    The XML node corresponding to the root Action Category is passed
    in xml_menu.
    """
    for dom_item in dom_menu.select("li"):
        menu_item_title = dom_item.select("a/text()")[0].extract_unquoted().strip()
        menu_item_id = normalize(menu_item_title)
        xml_menu_item = E.object(
            {
                'name': menu_item_id,
                'meta_type': 'CMF Action',
            },
            E.property(
                {'name': 'title'},
                menu_item_title),
        )
        try:
            xml_menu_item.append(E.property(
                {'name': 'url_expr'},
                'string:%s' % dom_item.select("a/@href")[0].extract().strip(),
            ))
        except IndexError:
            pass

        xml_menu.append(xml_menu_item)

        try:
            dom_sub_menu = dom_item.select("ul")[0]
            xml_sub_menu = E.object(
                {
                    'name': menu_item_id + '_sub',
                    'meta_type': 'CMF Action Category',
                }
            )
            menu_dom2actionsXML(dom_sub_menu, xml_sub_menu)
            xml_menu.append(xml_sub_menu)
        except IndexError:
            pass


class QuickLink(Item):
    title = Field()
    url = Field()


class GlobalNavigation(Item):
    root = Field()


class MacNavigationScraper(BaseSpider):
    name = "McMaster global navigation scraper"
    allowed_domains = ["fhs.mcmaster.ca"]
    start_urls = [
        "http://fhs.mcmaster.ca/lib/library/mcmaster_global_navigation_accessibility.lbi",
        "http://fhs.mcmaster.ca/",
    ]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)

        # Extract the quicklinks
        for dom_qlink in hxs.select("//select[@name='quick_links']/option[@value!='#']"):
            yield QuickLink(**{
                'title': dom_qlink.select("text()").extract_unquoted()[0],
                'url':   dom_qlink.select("@value").extract()[0],
            })
        # Extract the global navigation menu
        for dom_menu in hxs.select("/html/body/ul[@class='sf-menu']"):
            yield GlobalNavigation(root=dom_menu)


class McMasterNavigationActionsXMLPipeline(object):
    """Process the navigation items found into an actions.xml format.
    """

    def open_spider(self, spider):
        try:
            # We try to graft the quick links and nav menu into the
            # existing actions.xml in order to preserve other elements
            # that might have been added.
            actions_file = open(spider.actions_file, 'r')
            actions_xml = lxml.etree.parse(actions_file)
        except IOError:
            # No file, create template XML
            from lxml.etree import ElementTree
            actions_xml = ElementTree(
                E.object({'name': 'portal_actions',
                          'meta_type': 'Plone Actions Tool',
                          },
                         E.object({'name': 'mcmaster_globalnav',
                                   'meta_type': 'CMF Action Category',
                                   }),
                         E.object({'name': 'mcmaster_quick_links',
                                   'meta_type': 'CMF Action Category',
                                   }),
                         )
            )
        self.actions_xml = actions_xml

        # Start the new quick links list
        self.quick_links = self.actions_xml.find('//object[@name="mcmaster_quick_links"]')
        attrs = dict(self.quick_links.attrib)
        self.quick_links.clear()
        self.quick_links.attrib.update(attrs)

        # Start the new global navigation menu
        self.globalnav = actions_xml.find('//object[@name="mcmaster_globalnav"]')
        attrs = dict(self.globalnav.attrib)
        self.globalnav.clear()
        self.globalnav.attrib.update(attrs)

    def close_spider(self, spider):
        proc = subprocess.Popen(
            ['xmllint', '--format', '/dev/stdin'],
            stdin=subprocess.PIPE,
            stdout=open(spider.actions_file, 'w'),
        )
        self.actions_xml.write(proc.stdin,
                               xml_declaration=False,
                               pretty_print=False,
                               )
        (_, stderr) = proc.communicate()

        if stderr is not None:
            raise subprocess.CalledProcessError(stderr)

    def process_item(self, item, spider):
        if isinstance(item, QuickLink):
            self.process_quick_link(item)
        elif isinstance(item, GlobalNavigation):
            self.process_globalnav(item['root'])
        else:
            raise DropItem('Unkown item type.', item)

    def process_quick_link(self, quick_link):
        self.quick_links.append(
            E.object(
                {
                    'name': normalize(quick_link['title']),
                    'meta_type': 'CMF Action',
                },
                E.property(
                    {'name': 'title'},
                    quick_link['title']),
                E.property(
                    {'name': 'url_expr'},
                    'string:%s' % quick_link['url'],
                )
            )
        )

    def process_globalnav(self, dom_menu):
        menu_dom2actionsXML(dom_menu, self.globalnav)


scrapy.settings.default_settings.ITEM_PIPELINES = [
    'scrape_global_nav.McMasterNavigationActionsXMLPipeline',
]

SPIDER = MacNavigationScraper()
