Introduction
============

    This package provides the McMaster University baseline
    web-branding for a Plone 4 site.

    The skin is set as the default for the website upon
    installation. The Plone interface is kept for authoring, while
    using the McMaster branding compliant design for the public
    interface.

Origin

    This a successor of McMasterBranding, and takes a lot from
    it. McMasterBranding is a product with the same function but for
    Plone 2.5 developed at Career Services.

Credits

    Servilio Afre Puentes <afrepues@mcmaster.ca>
    Sergio Venier <veniers@mcmaster.ca>
    Diego Benavides 
