from setuptools import setup, find_packages
import os

version = '1.5.7'

setup(name='mcmaster.branding.theme',
      version=version,
      description="Proper branding for a McMaster University website",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
          'Framework :: Plone',
          'Programming Language :: Python',
          'Topic :: Software Development :: Libraries :: Python Modules',
      ],
      keywords='web zope plone theme branding mcmaster university',
      author='Servilio Afre Puentes',
      author_email='afrepues@mcmaster.ca',
      url='http://develz.mcmaster.ca/webdev/',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['mcmaster', 'mcmaster.branding'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
          'Products.CMFPlone >=4.2,<5.0',
          'quintagroup.dropdownmenu',
      ],
      entry_points={
          'z3c.autoinclude.plugin': ['target = plone'],
      },
      )
